/*
 * main.c
 *
 *  Created on: Sep 5, 2018
 *      Author: ryan
 */

#include "xparameters.h"
#ifndef MODEX
#include "xllfifo.h"
#include "xgpio.h"
#include "xstatus.h"
#include "xuartlite.h"
#include "xuartlite_l.h"
#include "xtmrctr.h"
#endif

// Function definitions
/* Monitor and Recovery control functions */
int Monitor(int16_t *data, int num_vals);
void RCF(int16_t *data);

#define MonitorData(a, b) Monitor(a, b)
#define MonitorCmds(a, b) Monitor(a, b)

/* Sensor functions */
void RecvData(XUartLite *uart_data, int16_t *data);
void SendData(XLlFifo *fifo_data, int16_t *data);

/* Actuator functions */
int RecvCmds(XLlFifo *fifo_cmds, int16_t *cmds, XTmrCtr *wd_timer);
void SendCmds(XUartLite *uart_cmds, int16_t *cmds);

/* Setup functions */
int UartSetup(XUartLite *uart, u32 id);
int FifoSetup(XLlFifo *fifo, u32 id);

#define FIFO_CMDS_DEV_ID XPAR_RX_MOSI_FIFO_MM_S_0_DEVICE_ID
#define FIFO_DATA_DEV_ID XPAR_TX_MISO_FIFO_MM_S_0_DEVICE_ID

#define UART_DATA_DEV_ID XPAR_AXI_UARTLITE_1_DEVICE_ID
#define UART_CMDS_DEV_ID XPAR_AXI_UARTLITE_0_DEVICE_ID

#define MESSAGE_SIZE 12
#define NUM_DATA_VALS 2
#define NUM_CMDS_VALS 2

#define AXI_gpio_cmds XPAR_AXI_GPIO_1_DEVICE_ID
#define AXI_gpio_data XPAR_AXI_GPIO_2_DEVICE_ID

#define WD_PERIOD 50
#define TICKS_TO_MS 100000
#define WD_TIMER_ID XPAR_AXI_TIMER_0_DEVICE_ID

//#define DEBUG_LED

#ifndef MODEX
#ifdef DEBUG_LED
	static inline void LEDSet(XGpio *a, unsigned b, u32 c) { XGpio_DiscreteWrite(a, b, c); }
	XGpio gpio_cmds;
	XGpio gpio_data;
#else
	static inline void LEDSet(int *a, int b, int c) { }
#endif
#endif

#ifdef MODEX
#define XTC_AUTO_RELOAD_OPTION 0
#endif
static int16_t SAFETY_VALS[NUM_CMDS_VALS];

int main() {
	/* GPIO setup */
#ifdef DEBUG_LED

	/* Commands */
	u32 rgb = 0x0;
	XGpio_Initialize(&gpio_cmds, AXI_gpio_cmds);
	XGpio_SetDataDirection(&gpio_cmds, 1, 0x0000);

	/* Data */
	u32 led = 0x0;
	XGpio_Initialize(&gpio_data, AXI_gpio_data);
	XGpio_SetDataDirection(&gpio_data, 1, 0x0000);
#else
	int gpio_cmds = 0, gpio_data = 0;
	int rgb = 0, led = 0;
#endif
	int retval, i;

    for (i = 0; i < NUM_CMDS_VALS; i++) {
        SAFETY_VALS[i] = 0;
    }

	/* UART setup */
	/* Commands out */
	XUartLite uart_cmds;
	retval = UartSetup(&uart_cmds, UART_CMDS_DEV_ID);
    if (retval) {
		return retval;
	}
	LEDSet(&gpio_cmds, 1, ++rgb);

	/* Data in */
	XUartLite uart_data;
	retval = UartSetup(&uart_data, UART_DATA_DEV_ID);
    if (retval) {
		return retval;
	}
	LEDSet(&gpio_data, 1, ++led);


	/* FIFO setup */
	/* intialize device config interface */
	/* Commands */
	XLlFifo fifo_cmds;
	retval = FifoSetup(&fifo_cmds, FIFO_CMDS_DEV_ID);
    if (retval) {
		return retval;
	}
	LEDSet(&gpio_cmds, 1, ++rgb);

	/* Data */
	XLlFifo fifo_data;
	retval = FifoSetup(&fifo_data, FIFO_DATA_DEV_ID);
    if (retval) {
		return retval;
	}
	LEDSet(&gpio_data, 1, ++led);

	/* TIMER setup */
	XTmrCtr wd_timer;
	XTmrCtr_Initialize(&wd_timer, WD_TIMER_ID);
	XTmrCtr_SetOptions(&wd_timer, WD_TIMER_ID, XTC_AUTO_RELOAD_OPTION);

	XTmrCtr_Start(&wd_timer, WD_TIMER_ID);

	while (1) {
        int data_invalid, wd_triggered;
		int16_t data[NUM_DATA_VALS], cmds[NUM_CMDS_VALS];
        data_invalid = 0;
        wd_triggered = 0;

		// read in data from uart
		RecvData(&uart_data, data);
#ifdef MODEX
DATA_RECEIVED:
#endif
		// reset receiving data line
		XLlFifo_RxReset(&fifo_cmds);

		data_invalid = MonitorData(data, NUM_DATA_VALS);
		SendData(&fifo_data, data);
		LEDSet(&gpio_data, 1, ++led);

		// read cmds from fifo
		wd_triggered = RecvCmds(&fifo_cmds, cmds, &wd_timer);

		if (wd_triggered || data_invalid || MonitorCmds(cmds, NUM_CMDS_VALS)) {
#ifdef MODEX
RCF_ACTIVATED:
#endif
			RCF(cmds);
		}
		SendCmds(&uart_cmds, cmds);
#ifdef MODEX
COMMANDS_SENT:
#endif
		LEDSet(&gpio_cmds, 1, ++rgb);
	}
#ifdef MODEX
STATE_ERROR:
    skip;
#endif
}

/* Uart Setup */
int UartSetup(XUartLite *uart, u32 id) {
	int status;
	status = XUartLite_Initialize(uart, id);
	if (status != XST_SUCCESS) {
		return -1;
	}

	status = XUartLite_SelfTest(uart);
	if (status != XST_SUCCESS) {
		return -2;
	}
	return 0;
}

/* Fifo setup */
int FifoSetup(XLlFifo *fifo, u32 id) {
	int status;
	XLlFifo_Config *config;
	config = XLlFfio_LookupConfig(id);
	if (!config) {
		return -3;
	}

	status = XLlFifo_CfgInitialize(fifo, config, config->BaseAddress);
	if (status != XST_SUCCESS) {
		return status;
	}

	/* check the reset value */
	status = XLlFifo_Status(fifo);
	XLlFifo_IntClear(fifo, 0xffffffff);
	status = XLlFifo_Status(fifo);
	if (status) {
		return status;
	}

	// reset fifo
	XLlFifo_Reset(fifo);
	return 0;
}

#define MIN_VAL -2048 // -64.0 cast as a fixed point
#define MAX_VAL 2016  // 63.0 cast as a fixed point

/* monitor function */
/* returns > 0 if violation occurs, else 0 */
int Monitor(int16_t *values, int num_vals) {
	int violation, i;
    violation = 0;

	for (i = 0; i < num_vals; i++) {
		if (values[i] > MAX_VAL || values[i] < MIN_VAL) {
			violation++;
		}
	}

	return violation;
}


/* RCF function */
/* modifies output string to set to safety value */
void RCF(int16_t *values) {
    int i;
	for (i = 0; i < NUM_CMDS_VALS; i++) {
		values[i] = SAFETY_VALS[i];
	}
}

/* Receive data from uart, return the number of values received */
void RecvData(XUartLite *uart_data, int16_t *data) {
	int8_t tmp;
	int count = 0;
	int write_len = 0, len = 0;

	memset(data, 0, NUM_DATA_VALS * sizeof(*data));
	while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
	len |= (((int16_t)tmp) & 0x00FF);
	while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
	len |= (((int16_t)tmp << 8) & 0xFF00);
	if (len > NUM_DATA_VALS) {
        write_len = NUM_DATA_VALS;
    } else {
        write_len = len;
    }
	for (count = 0; count < write_len; count++) {
		while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
		data[count] |= (((int16_t)tmp) & 0x00FF);
		while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
		data[count] |= (((int16_t)tmp << 8) & 0xFF00);
	}
	// toss extra data
	while (count < len) {
        while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
		while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
        count++;
	}
}

/* Send data to the AP */
void SendData(XLlFifo *fifo_data, int16_t *data) {
	int write_len = NUM_DATA_VALS;
    if (write_len % 2) {
        write_len += 1;
    }
	write_len *= 2;

	// write out data to fifo
	XLlFifo_Write(fifo_data, data, write_len);
	XLlFifo_TxSetLen(fifo_data, write_len);
	while(!(XLlFifo_IsTxDone(fifo_data)));
	XLlFifo_IntClear(fifo_data, 0xffffffff);
}

/* receive commands from the AP write them to cmds array */
int RecvCmds(XLlFifo *fifo_cmds, int16_t *cmds, XTmrCtr *wd_timer) {
	int read_len = 0;
	int len;
	unsigned int start, stop;

	start = XTmrCtr_GetValue(wd_timer, WD_TIMER_ID) / TICKS_TO_MS;
	// Read in data from fifo
	while (!(XLlFifo_IsRxDone(fifo_cmds))) {
		stop = XTmrCtr_GetValue(wd_timer, WD_TIMER_ID) / TICKS_TO_MS;
		if (stop > (start + WD_PERIOD)) {
			return 1;
		}
	}
	len = XLlFifo_RxGetLen(fifo_cmds);
    if (len > NUM_CMDS_VALS * sizeof(*cmds)) {
        read_len = NUM_CMDS_VALS * sizeof(*cmds);
    } else {
        read_len = len;
    }
	XLlFifo_Read(fifo_cmds, cmds, read_len);
	// toss the extra bytes
	if (len > read_len) {
		XLlFifo_RxReset(fifo_cmds);
	}
	XLlFifo_IntClear(fifo_cmds, 0xffffffff);
	return 0;
}

/* Send commands to the actuators */
void SendCmds(XUartLite *uart_cmds, int16_t *cmds) {
	int count;
	int8_t tmp;
    tmp = NUM_CMDS_VALS;
	while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
	tmp = (NUM_CMDS_VALS >> 8);
	while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
	// write out data to uart, always null terminating
	for (count = 0; count < NUM_CMDS_VALS; count++) {
		tmp = cmds[count];
		while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
		tmp = (cmds[count] >> 8);
		while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
	}
}

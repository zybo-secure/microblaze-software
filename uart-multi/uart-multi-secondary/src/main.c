/*
 * main.c
 *  Secondary TIOP
 *  Created on: Sep 5, 2018
 *      Author: ryan
 */

#include "xparameters.h"
#ifndef MODEX
#include "xstatus.h"
#include "xuartlite.h"
#include "xuartlite_l.h"
#include "mb_interface.h"
#endif

// Function definitions

/* Sensor functions */
void RecvData(XUartLite *uart_data, int16_t *data);
void SendData(int16_t *data);

/* Actuator functions */
void RecvCmds(int16_t *cmds);
void SendCmds(XUartLite *uart_cmds, int16_t *cmds);

/* Setup functions */
int UartSetup(XUartLite *uart, u32 id);

#define UART_DATA_DEV_ID XPAR_AXI_UARTLITE_1_DEVICE_ID
#define UART_CMDS_DEV_ID XPAR_AXI_UARTLITE_0_DEVICE_ID

#define MESSAGE_SIZE 12
#define NUM_DATA_VALS 2
#define NUM_CMDS_VALS 2

int main() {
	/* GPIO setup */
	int retval;

	/* UART setup */
	/* Commands out */
	XUartLite uart_cmds;
	retval = UartSetup(&uart_cmds, UART_CMDS_DEV_ID);
    if (retval) {
		return retval;
	}

	/* Data in */
	XUartLite uart_data;
	retval = UartSetup(&uart_data, UART_DATA_DEV_ID);
    if (retval) {
		return retval;
	}


	while (1) {
		int16_t data[NUM_DATA_VALS], cmds[NUM_CMDS_VALS];

		// read in data from uart
		RecvData(&uart_data, data);

		SendData(data);

		// read cmds from fifo
		RecvCmds(cmds);

		SendCmds(&uart_cmds, cmds);
	}
}

/* Uart Setup */
int UartSetup(XUartLite *uart, u32 id) {
	int status;
	status = XUartLite_Initialize(uart, id);
	if (status != XST_SUCCESS) {
		return -1;
	}

	status = XUartLite_SelfTest(uart);
	if (status != XST_SUCCESS) {
		return -2;
	}
	return 0;
}

/* Receive data from uart, return the number of values received */
void RecvData(XUartLite *uart_data, int16_t *data) {
	int8_t tmp;
	int count = 0;
	int write_len = 0, len = 0;

	memset(data, 0, NUM_DATA_VALS * sizeof(*data));
	while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
	len |= (((int16_t)tmp) & 0x00FF);
	while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
	len |= (((int16_t)tmp << 8) & 0xFF00);
	if (len > NUM_DATA_VALS) {
        write_len = NUM_DATA_VALS;
    } else {
        write_len = len;
    }
	for (count = 0; count < write_len; count++) {
		while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
		data[count] |= (((int16_t)tmp) & 0x00FF);
		while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
		data[count] |= (((int16_t)tmp << 8) & 0xFF00);
	}
	// toss extra data
	while (count < len) {
        while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
		while(!XUartLite_Recv(uart_data, (u8 *)&tmp, 1));
        count++;
	}
}

/* Send data to the AP */
void SendData(int16_t *data) {
	int32_t tmp;
	for (int i = 0; i < NUM_DATA_VALS; i++) {
		tmp = (int32_t)data[i];
		putfsl(tmp,0);
	}
}

/* receive commands from the AP write them to cmds array */
void RecvCmds(int16_t *cmds) {
	int32_t tmp;
	for (int i = 0; i < NUM_CMDS_VALS; i++) {
		getfsl(tmp, 0);
		cmds[i] = (int16_t) tmp;
	}
}

/* Send commands to the actuators */
void SendCmds(XUartLite *uart_cmds, int16_t *cmds) {
	int count;
	int8_t tmp;
    tmp = NUM_CMDS_VALS;
	while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
	tmp = (NUM_CMDS_VALS >> 8);
	while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
	// write out data to uart, always null terminating
	for (count = 0; count < NUM_CMDS_VALS; count++) {
		tmp = cmds[count];
		while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
		tmp = (cmds[count] >> 8);
		while(!XUartLite_Send(uart_cmds, (u8 *)&tmp, 1));
	}
}
